# Flight Search

- To search flights from source to destination

## Execution Steps 

- Run following command from project directory path 

go run main.go

## Login API
- Select POST method and specify - http://localhost:4700/o/login
- Use following Credentials For Authentication 

**Input**
{
	  "username": "Admin",
      "password": "Admin@123"
}
- Above API will generate token which will be used to find flights

## All Flights API
- It will list all flights 
- Select GET Method - http://localhost:4700/o/flights




## Search Flight API
- Search origin to destination flights 
- As API is restricted , will need to provide login Credentials to get flights
- Select POST method and specify - http://localhost:4700/r/searchflights
- Provide token in Headers as
Authorization :  token 

**Input**
{
	  "origin": "Mumbai",
      "destination": "Delhi"
}


- For return flights just reverse destination and origin values 

**Input**
{
	  "origin": "Delhi",
      "destination": "Mumbai"
}


